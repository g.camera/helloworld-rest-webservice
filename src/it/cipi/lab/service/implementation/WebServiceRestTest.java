package it.cipi.lab.service.implementation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("testwebservice")
public class WebServiceRestTest {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("helloworld")
	public String helloWorld () {
		String helloWorldRsponse = "hello world";
		return helloWorldRsponse;
	}
	
	

}
